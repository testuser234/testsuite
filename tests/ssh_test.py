import os
import sys
sys.path.insert(0, 'ssh')

import ssh

def test_create_connection():
    result = ssh.create_connection()
    stdin, stdout, stderr = result.exec_command('echo "$USER"')
    assert stdout.read().strip('\n') == os.environ['SSH_USER']
    
def test_upload_file():
  target_path = "/home/commandemy/log.txt"
  assert ssh.upload_file('log.txt', target_path) == True
  assert ssh.file_exist(target_path) == True
  
def test_parse_log():
    target_path = "/home/commandemy/log.txt"
    assert "EULA has been accepted" in ssh.file_read(target_path)
    assert "Filesystem is mounted" in ssh.file_read(target_path)
    assert "Finished. Syncing..." in ssh.file_read(target_path)