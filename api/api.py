"""A module that interacts with the Gitlab API"""
import os
import requests

BASE_URL = "https://gitlab.com/api/v3"

def login():
  """Logs a user into Gitlab"""
  return requests.post(BASE_URL + '/session?login=' + os.environ['GITLAB_USER'] + '&password=' + os.environ['GITLAB_PW'])
  
def project_list():
  """List all projects for logged in user"""
  headers = {'PRIVATE-TOKEN': 'zoTsxnFCxqzKPGRaFRPx'}
  return requests.get(BASE_URL + '/projects', headers=headers)