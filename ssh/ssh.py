"""A module that interacts with the SSH"""
import os
import paramiko

def create_connection():
    """Create a SSH connection if possible"""
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(
            os.environ['SSH_HOST'],
            username=os.environ['SSH_USER'],
            password=os.environ['SSH_PW']
        )
        return ssh
    except Exception as e:
        print e
        return False
        
def upload_file(source_path, target_path):
    """Uploads a file from the source_path via SSH to the target_path"""
    try:
        ssh = create_connection()
        sftp = ssh.open_sftp()
        sftp.put(source_path, target_path)
        sftp.close()
        ssh.close()
        return True
    except Exception as e:
        print e
        return False

def file_exist(target_path):
    """Check if file exists at the target_path"""
    try:
        ssh = create_connection()
        sftp = ssh.open_sftp()
        sftp.stat(target_path)
        sftp.close()
        ssh.close()
    except IOError, e:
        if 'No such file' in str(e):
            return False
        raise
    else:
        return True  
      
def file_read(target_path):
  """Read a file via SSH"""
  try:
      ssh = create_connection()
      sftp = ssh.open_sftp()

      file = sftp.open(target_path)
      content = file.read()
      file.close

      sftp.close()
      ssh.close()

      return content
  except Exception as e:
      print e
      return False