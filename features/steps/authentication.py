import os

@given(u'I have access to Gitlab')
def step_impl(context):
    context.browser.get('https://gitlab.com/users/sign_in')

@when(u'I login with my credentials')
def step_impl(context):
    context.browser.find_element_by_id("user_login").send_keys(os.environ['GITLAB_USER'])
    context.browser.find_element_by_id("user_password").send_keys(os.environ['GITLAB_PW'])
    context.browser.find_element_by_name("commit").click()

@then(u'I should be logged in')
def step_impl(context):
    assert "Signed in successfully." in context.browser.page_source 